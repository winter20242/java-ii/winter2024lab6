public class LuckyCardGameApp {
    
    public static void main(String[] args) {
        int totalPoints = 0;
        GameManager manager = new GameManager();

        System.out.println("Welcome Loser");
        int round = 0;

        while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
            round ++;
            System.out.println("******************************************************");
            System.out.println("Round " + round);
            System.out.println("---------------------------");
            System.out.println(manager);
            System.out.println("---------------------------");
            totalPoints = totalPoints + manager.calculatePoints();
            System.out.println("You have " + totalPoints + " points!");
            manager.dealCards();
        }
        if (totalPoints < 5)
            System.out.println("You lost, Loser");
        else
            System.out.println("You win, only by luck, because you're still a loser");
    }

}