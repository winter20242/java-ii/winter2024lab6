public class Card {
    private String suit;
    private int value;

    public Card(String suit, int value) {
        this.suit = suit;
        this.value = value;
    }

    public String getSuit() {
        return this.suit;
    }

    public int getValue() {
        return this.value;
    }

    public String toString() {
        String printValue = Integer.toString(value);
        if (value == 1)
            printValue = "Ace";
        if (value == 11)
            printValue = "Jack";
        if (value == 12)
            printValue = "Queen";
        if (value == 13)
            printValue = "King";

        return printValue + " of " + suit;
    }

}
