public class GameManager {
    
    Deck drawPile;
    Card centerCard;
    Card playeCard;

    public GameManager() {
        this.drawPile = new Deck();
        drawPile.shuffle();
        centerCard = drawPile.drawTopCard();
        playeCard = drawPile.drawTopCard();
    }

    public String toString() {
        return "Center card: " + centerCard + "\nPlayer card: " + playeCard;
    }

    public void dealCards() {
        drawPile.shuffle();
        centerCard = drawPile.drawTopCard();
        playeCard = drawPile.drawTopCard();
    }

    public int getNumberOfCards() {
        return drawPile.numOfCards;
    }

    public int calculatePoints() {
        int points = 0;
        if (centerCard.getValue() == playeCard.getValue())
            points = points + 4;
        if (centerCard.getSuit() == playeCard.getSuit()) 
            points = points + 2;
        else
            points--;

        return points;
    }

}